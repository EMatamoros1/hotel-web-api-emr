INSERT INTO categoria_cuarto (nombre,descripcion,precio)
    VALUES ('Individual', 'Ideal para quienes viajan solos.',50.0);

INSERT INTO cuarto (numero, descripcion,categoria)
    VALUES(1,'Vista a la piscina',1);
INSERT INTO cuarto (numero, descripcion,categoria)
  VALUES(2,'Remodelado recientemente',1);


INSERT INTO huesped (nombre , email ,telefono )
    VALUES('ERNESTO MAT','em@email.com', '00-01-00-22');

INSERT INTO huesped (nombre , email ,telefono )
    VALUES('JUAN  PEREZ','jpz@email.com', '00-01-00-22');

INSERT INTO huesped (nombre , email ,telefono )
    VALUES('ROSA MARIA','rm@email.com', '00-01-00-22');

INSERT INTO huesped (nombre , email ,telefono )
    VALUES('JUANA PAIZ','jp@email.com', '00-01-00-22');


--INSERT RESERVACIONES

INSERT INTO reservacion (desde, hasta,cuarto,huesped)
    VALUES ({ts '2017-02-15 18:47:52.69'},{ts '2015-02-17 18:47:52.69'},1,1);

INSERT INTO reservacion (desde, hasta,cuarto,huesped)
    VALUES ({ts '2017-03-01 18:47:52.69'},{ts '2015-03-14 18:47:52.69'},1,1);

INSERT INTO reservacion (desde, hasta,cuarto,huesped)
    VALUES ({ts '2017-02-15 18:47:52.69'},{ts '2015-02-17 18:47:52.69'},2,2);

INSERT INTO reservacion (desde, hasta,cuarto,huesped)
    VALUES ({ts '2017-02-15 18:47:52.69'},{ts '2015-02-17 18:47:52.69'},1,1);

INSERT INTO reservacion (desde, hasta,cuarto,huesped)
    VALUES ({ts '2017-03-01 18:47:52.69'},{ts '2015-03-30 18:47:52.69'},2,2);

