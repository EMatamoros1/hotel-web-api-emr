/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.service;

import java.sql.Timestamp;
import java.util.List;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Reservacion;

/**
 *
 * @author ematamoros
 */
public interface ReservacionService {
    
    public void agregarReservacion(final Reservacion pReservacion) ;
    
    public void modificarReservacion(final Reservacion pReservacion) ;
    
   public Reservacion obteneReservacionId(final int pId) ;
   
   public Huesped obteneHuesped(final int pId) ;
    
    public void eliminarReservacion(final int pId) ;
            
     public Pagina<Reservacion> obtenerTodosReservaciones(Filtro paginacion) ;
       
    public Pagina<Cuarto> obtenerCupos(Timestamp desde, Timestamp hasta,final Filtro paginacion);
}
