/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.serviceImpl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.dao.HuespedDAO;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.service.ReservacionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ematamoros
 */

@Service
public class ReservacionServiceImpl implements ReservacionService {
    
    private final ReservacionDAO reservacionDAO;
     private final HuespedDAO huespedDAO;
    
     public ReservacionServiceImpl(final ReservacionDAO reservacionDAO , final HuespedDAO huespedDAO)   {
        this.reservacionDAO = reservacionDAO;
        this.huespedDAO=huespedDAO;
           }
     
     
    @Transactional
    @Override
    public void agregarReservacion(final Reservacion pReservacion) 
    {
        this.reservacionDAO.agregar(pReservacion);
    }
    
     @Transactional
     @Override
    public Reservacion obteneReservacionId(final int pId) 
    {
        return this.reservacionDAO.obtenerPorId(pId);
    }
    
     @Transactional
     @Override
    public Huesped obteneHuesped(final int pId) 
    {
        return this.huespedDAO.obtenerHuespedPorId(pId);
    }
    
    
    @Transactional
    @Override
    public void modificarReservacion(final Reservacion pReservacion) 
    {
        if(pReservacion.getId() < 1)
        {
            throw new IllegalArgumentException("Reservacion no existe");
        }
        this.reservacionDAO.guardar(pReservacion);
    }

    @Transactional
    @Override
    public void eliminarReservacion(final int pId) 
    {
        if(pId < 1)
        {
            throw new IllegalArgumentException("ID invalido. Debe ser mayor a cero");
        }
        this.reservacionDAO.eliminar(pId);
    }
     
    @Transactional
    @Override
    public Pagina<Cuarto> obtenerCupos( Timestamp desde, Timestamp hasta,final Filtro paginacion  ) 
    {
        List<Cuarto> cuartoDisponibles;
        final int count = this.reservacionDAO.contar();
        
        if(count > 0)
        {
            cuartoDisponibles = this.reservacionDAO.obtenerCuartosDisponibles(desde, hasta, paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
            cuartoDisponibles = new ArrayList<Cuarto>();
        }
         return new Pagina<Cuarto>(cuartoDisponibles, count,  paginacion.getOffset(), paginacion.getLimit());
    }
          
    
    @Transactional
    @Override
    public Pagina<Reservacion> obtenerTodosReservaciones(Filtro paginacion) 
    {
        List<Reservacion> reservaciones;
        final int count = this.reservacionDAO.contar();
        
        if(count > 0)
        {
            reservaciones = this.reservacionDAO.obtenerTodos(paginacion.getOffset(),
                    paginacion.getLimit());
        }
        else
        {
            reservaciones = new ArrayList<Reservacion>();
        }
         return new Pagina<Reservacion>(reservaciones, count,  paginacion.getOffset(), paginacion.getLimit());
    }
    
}
