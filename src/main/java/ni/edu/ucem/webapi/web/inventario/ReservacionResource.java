/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.web.inventario;

import java.sql.Timestamp;
import java.util.Date;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ni.edu.ucem.webapi.core.ApiResponse;
import ni.edu.ucem.webapi.core.ApiResponse.Status;
import ni.edu.ucem.webapi.core.ListApiResponse;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Reservacion;
import ni.edu.ucem.webapi.modelo.Pagina;
import ni.edu.ucem.webapi.modelo.Filtro;
import ni.edu.ucem.webapi.modelo.Huesped;
import ni.edu.ucem.webapi.serviceImpl.InventarioServiceImpl;
import ni.edu.ucem.webapi.serviceImpl.ReservacionServiceImpl;


/**
 *
 * @author ematamoros
 */

@RestController
@RequestMapping("/v1/inventario/reservacion")
public class ReservacionResource {
    
     private final ReservacionServiceImpl reservacionService;
     private final InventarioServiceImpl inventarioService;
    
    @Autowired
    public ReservacionResource(final ReservacionServiceImpl reservacionService,
            final InventarioServiceImpl inventarioService )
    {
        this.reservacionService = reservacionService;
        this.inventarioService=inventarioService;
        
    }
    
    
   @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces="application/json")
    public ApiResponse obtener(@PathVariable("id") final int id)
    {
        final Reservacion reservacion = this.reservacionService.obteneReservacionId(id);
        return new ApiResponse(Status.OK, reservacion);
    }
    
     @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ApiResponse guardarReservacionConFormData(final Timestamp desde, final Timestamp hasta ,final int cuartoId, final int  huespedId) 
    {
        
        //Add DAO huesped y  
        
        final Cuarto cuarto=this.inventarioService.obtenerCuarto(cuartoId);
        final Huesped huesped=this.reservacionService.obteneHuesped(huespedId);
       
        final Reservacion reservacion = new Reservacion(desde, hasta, cuarto,huesped);
        this.reservacionService.agregarReservacion(reservacion);
        return new ApiResponse(Status.OK, reservacion);
    }
    
    
    @RequestMapping(method = RequestMethod.GET, produces="application/json")
    public ListApiResponse<Cuarto> obtenerCupos(
            @RequestParam(value = "desde", required = false, defaultValue ="0") final Timestamp desde,
            @RequestParam(value = "hasta", required = false, defaultValue="0") final Timestamp hasta,
            @RequestParam(value = "offset", required = false, defaultValue ="0") final Integer offset,
            @RequestParam(value = "limit", required = false, defaultValue="0") final Integer limit)
    {
        final Filtro paginacion = new Filtro.Builder()
                .paginacion(offset, limit)
                .build();

        Pagina<Cuarto> pagina;
       
          pagina = this.reservacionService.obtenerCupos(desde ,hasta , paginacion);
        
       
        return new ListApiResponse<Cuarto>(Status.OK, pagina);
    
    
    }



}
