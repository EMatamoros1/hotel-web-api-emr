/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.daoImpl;


import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;



import ni.edu.ucem.webapi.dao.ReservacionDAO;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Reservacion;

 /*/
 * @author ematamoros
 */

@Repository
public class ReservacionDAOImpl implements ReservacionDAO {
    
    
    private final JdbcTemplate jdbcTemplate;
   
    @Autowired
    public ReservacionDAOImpl(final JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }
    
       
     @Override
    public Reservacion obtenerPorId(final int pId) 
    {
        final String sql = "select * from reservacion where id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{pId}, 
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }
    
    @Override
    public List<Reservacion> obtenerTodos(final int pOffset, final int pLimit) 
    {
        String sql = "select * from reservacion offset ? limit ?";
        return this.jdbcTemplate.query(sql, new Object[]{pOffset, pLimit},
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
    }

    
    
    
    @Override
    public List<Reservacion> obtenerTodos() 
    {
        final String sql = "select * from reservacion";
         return this.jdbcTemplate.query(sql,
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
     }
    
    
    
        
     @Override
     public List<Reservacion> obtenerDatosxCampos(final String fields){
          final String sql = "select"  + fields +  " from reservacion ";
          
         return this.jdbcTemplate.query(sql,
                new BeanPropertyRowMapper<Reservacion>(Reservacion.class));
           
     }
     
       @Override
    public void agregar(final Reservacion pReservacion) 
    {
        final String sql = new StringBuilder()
                .append("INSERT INTO reservacion")
                .append(" ")
                .append("(desde, hasta, cuarto, huesped)")
                .append(" ")
                .append("VALUES (?, ?, ?,? )")
                .toString();
        final Object[] parametros = new Object[4];
        parametros[0] = pReservacion.getDesde();
        parametros[1] = pReservacion.getHasta();
        parametros[2] = pReservacion.getCuarto().getId();
        parametros[3] = pReservacion.getHuesped().getId();
        this.jdbcTemplate.update(sql,parametros);
        
    }
      
    
    
    @Override
    public int contar()
    {
        final String sql = "select count(*) from reservacion";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }
    
    
    @Override
    public void guardar(final Reservacion pReservacion) 
    {        
       final String sql = new StringBuilder()
                .append("UPDATE reservacion")
                .append(" ")
                .append("set desde = ?")
                .append(",hasta = ?")
                .append(",cuarto = ?")
                .append(",huespef = ?")
                .append(" ")
                .append("where id = ?")
                .toString();
       
       final Object[] parametros = new Object[5];
        parametros[0] = pReservacion.getDesde();
        parametros[1] = pReservacion.getHasta();
        parametros[2] = pReservacion.getCuarto().getId();
        parametros[3] = pReservacion.getHuesped().getId();
        parametros[4] = pReservacion.getId();
            
        this.jdbcTemplate.update(sql,parametros);
    }

    @Override
    public void eliminar(final int pId) 
    {
        final String sql = "delete from reservacion where id = ?";
        this.jdbcTemplate.update(sql, new Object[]{pId});
    }
    
    
     @Override
     public List<Cuarto> obtenerCuartosDisponibles(Timestamp desde, Timestamp hasta,int pOffset, int pLimit){
         
         final String sql = new StringBuilder()
                .append("select * from cuarto ")
                .append("where  id not in ")
                .append("(")
                     .append("select distinct c.id from reservacione r ")
                     .append("  inner join cuarto as c on r.cuarto=c.id")
                     .append("   where  r.desde>= {ts  '?'}  and  r.hasta<={ts  '?'} ")
                 .append(")").toString();
         
          return this.jdbcTemplate.query(sql, new Object[]{desde, hasta},
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
    
            }
     
     @Override
     public List<Cuarto> obtenerCuartosReservados (Timestamp desde, Timestamp hasta,int pOffset, int pLimit){
         /*  select  r.* from   cuarto c  inner join  reservacion r on c.id=r.cuarto
                where  r.desde>= {ts  '2017-02-01'}  and  r.hasta<={ts  '2017-02-17'}  */
   
          final String sql = "select  c.* \n" +
                    " from   cuarto c  inner join  reservacion r on c.id=r.cuarto\n" +
                        "where  r.desde>= {ts  '?'}  and  r.hasta<={ts  '?'} ";
                    
          return this.jdbcTemplate.query(sql, new Object[]{desde, hasta},
                new BeanPropertyRowMapper<Cuarto>(Cuarto.class));
                  
           
     }
    
    
    
    
     
     
     
     
}
