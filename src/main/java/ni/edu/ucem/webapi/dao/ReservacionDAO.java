/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.ucem.webapi.dao;

import java.sql.Timestamp;
import java.util.List;
import ni.edu.ucem.webapi.modelo.CategoriaCuarto;
import ni.edu.ucem.webapi.modelo.Cuarto;
import ni.edu.ucem.webapi.modelo.Reservacion;

/**
 *
 * @author ematamoros
 */
public interface ReservacionDAO {
    
     public List<Reservacion> obtenerDatosxCampos(final String fields);
     
     public Reservacion obtenerPorId(final int pId) ;
     
     public List<Reservacion> obtenerTodos();
      
     public List<Reservacion> obtenerTodos(final int pOffset, final int pLimit);
  
     public void agregar(final Reservacion pReservacion) ;
     
     public void guardar(final Reservacion pReservacion) ;
     
     public void eliminar(final int pId) ;
     
     public List<Cuarto> obtenerCuartosReservados(Timestamp desde, Timestamp hasta,int pOffset, int pLimit);
     
     public List<Cuarto> obtenerCuartosDisponibles(Timestamp desde, Timestamp hasta,int pOffset, int pLimit);
     
      public int contar();
    

}
